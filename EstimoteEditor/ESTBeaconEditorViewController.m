//
//  ESTBeaconEditorViewController.m
//  Examples
//
//  Created by Tod Dooshik Chung on 5/16/14.
//  Copyright (c) 2014 com.estimote. All rights reserved.
//

#import "ESTBeaconEditorViewController.h"

@interface ESTBeaconEditorViewController ()



@end

@implementation ESTBeaconEditorViewController

@synthesize beacon;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [beacon connectToBeacon];
    
    uuidTextField.text=beacon.proximityUUID.UUIDString;
    majorTextField.text=[NSString stringWithFormat:@"%d",[beacon.major intValue]];
    minorTextField.text=[NSString stringWithFormat:@"%d",[beacon.minor intValue]];
}

-(IBAction)didTouchConnect:(id)sender{
    if(!beacon.isConnected){
        [beacon connectToBeacon];
        [connectionButton setTitle:@"Disconnect" forState:UIControlStateNormal];
    }
    else{
        [beacon disconnectBeacon];
        [connectionButton setTitle:@"Connect" forState:UIControlStateNormal];
    }
    
}

-(IBAction)didTouchUpdate:(id)sender{
    
    proximityWritingComplete=NO;
    majorWritingComplete=NO;
    minorWritingComplete=NO;
    
    if(beacon.isConnected){
        [beacon writeBeaconProximityUUID:uuidTextField.text
                           withCompletion:^(NSString *value, NSError *error) {
                               uuidTextField.text=value;
                               proximityWritingComplete=YES;
                               [self writingCompletionNotice];
                           }

         ];
        [beacon writeBeaconMajor:[majorTextField.text intValue] withCompletion:^(unsigned short value, NSError *error) {
            majorTextField.text=[NSString stringWithFormat:@"%d",value];
            majorWritingComplete=YES;
            [self writingCompletionNotice];
        }];
        [beacon writeBeaconMinor:[minorTextField.text intValue] withCompletion:^(unsigned short value, NSError *error) {
            minorTextField.text=[NSString stringWithFormat:@"%d",value];
            minorWritingComplete=YES;
            [self writingCompletionNotice];
        }];
    }
    else{
        [connectionButton setTitle:@"Connect" forState:UIControlStateNormal];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"먼저 비콘에 연결해야 합니다." message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]; [alert show];
    }
}

-(void)writingCompletionNotice{
    if(proximityWritingComplete && majorWritingComplete && minorWritingComplete){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Update Complete" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]; [alert show];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
