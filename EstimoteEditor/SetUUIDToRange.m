//
//  SetUUIDToRange.m
//  EstimoteEditor
//
//  Created by Tod Dooshik Chung on 5/20/14.
//  Copyright (c) 2014 Tod Dooshik Chung. All rights reserved.
//

#import "SetUUIDToRange.h"
#import "ESTBeaconManager.h"

@interface SetUUIDToRange ()

@end

@implementation SetUUIDToRange

@synthesize delegate;
@synthesize currentUUID;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"%s",__FUNCTION__);    
    uuid.text=currentUUID;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)applyOfferDropUUID:(id)sender{
    uuid.text=@"D1B024CB-A02D-4650-9C6A-BAEDA8A31F0E";
}

-(IBAction)applyEstimoteUUID:(id)sender{
    uuid.text=ESTIMOTE_PROXIMITY_UUID.UUIDString;
}

-(IBAction)cancel:(id)sender{
    [self dismissViewControllerAnimated:NO completion:nil];
}
-(IBAction)done:(id)sender{
    [self dismissViewControllerAnimated:NO completion:nil];
    [delegate didUpdateUUIDToRange:uuid.text];    
}



@end
