//
//  SetUUIDToRange.h
//  EstimoteEditor
//
//  Created by Tod Dooshik Chung on 5/20/14.
//  Copyright (c) 2014 Tod Dooshik Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SetUUIDToRangeDelegate
- (void)didUpdateUUIDToRange:(NSString*)UUIDString;
@end

@interface SetUUIDToRange : UIViewController{
    IBOutlet UITextField*uuid;
    id<SetUUIDToRangeDelegate> delegate;
    NSString*currentUUID;    
}
@property(nonatomic,retain) id<SetUUIDToRangeDelegate> delegate;
@property(nonatomic,retain) NSString*currentUUID;

-(IBAction)cancel:(id)sender;
-(IBAction)done:(id)sender;
-(IBAction)applyOfferDropUUID:(id)sender;
-(IBAction)applyEstimoteUUID:(id)sender;


@end
