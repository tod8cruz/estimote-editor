//
//  ESTBeaconTableVC.m
//  DistanceDemo
//
//  Created by Grzegorz Krukiewicz-Gacek on 17.03.2014.
//  Copyright (c) 2014 Estimote. All rights reserved.
//

#import "ESTBeaconTableVC.h"
#import "ESTBeaconManager.h"
#import "ESTBeaconEditorViewController.h"


@interface ESTBeaconTableVC () <ESTBeaconManagerDelegate>

@property (nonatomic, strong) ESTBeaconManager *beaconManager;
@property (nonatomic, strong) ESTBeaconRegion *region;
@property (nonatomic, strong) NSArray *beaconsArray;

@end
@implementation ESTBeaconTableVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    UUIDToMonitor=ESTIMOTE_PROXIMITY_UUID.UUIDString;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self startRanging];
}

-(void)startRanging{
    if([CLLocationManager isRangingAvailable]==YES){
        self.beaconManager = [[ESTBeaconManager alloc] init];
        self.beaconManager.delegate = self;
        
        NSUUID*uuid=[[NSUUID alloc]initWithUUIDString:UUIDToMonitor];
        self.region = [[ESTBeaconRegion alloc] initWithProximityUUID:uuid
                                                          identifier:@"EstimoteSampleRegion"];
        [self.beaconManager startRangingBeaconsInRegion:self.region];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bluetooth Low Energy를 지원하지 않습니다." message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]; [alert show];
    }
    
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    /*
     *Stops ranging after exiting the view.
     */
    [self.beaconManager stopRangingBeaconsInRegion:self.region];
}

#pragma mark - SetUUIDToRangeDelegate
- (void)didUpdateUUIDToRange:(NSString*)NewUUID{
    [self.beaconManager stopRangingBeaconsInRegion:self.region];
    UUIDToMonitor=NewUUID;
    [self startRanging];
}


#pragma mark - ESTBeaconManager delegate

- (void)beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region
{
    
    NSLog(@"%s",__FUNCTION__);
    self.beaconsArray = beacons;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return MAX([self.beaconsArray count],1);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    NSLog(@"%s",__FUNCTION__);
    /*
     * Fill the table with beacon data.
     */

    
    NSLog(@"%s: %d",__FUNCTION__,[self.beaconsArray count]);
    
    if([self.beaconsArray count]==0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"NoBeaconCell"];
    }
    else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        ESTBeacon *beacon = [self.beaconsArray objectAtIndex:indexPath.row];
        UILabel *beaconInfo = (UILabel *)[cell viewWithTag:1];
        UILabel *beaconDetail = (UILabel *)[cell viewWithTag:2];        
        beaconInfo.text = [NSString stringWithFormat:@"Major: %@, Minor: %@", beacon.major, beacon.minor];
        beaconDetail.text = [NSString stringWithFormat:@"Distance: %.2f", [beacon.distance floatValue]];
    }
    
    
    return cell;
}

#pragma mark - Table view delegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"didSelectBeacon"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        ESTBeaconEditorViewController *destViewController = segue.destinationViewController;
        destViewController.beacon = [self.beaconsArray objectAtIndex:[indexPath row]];
    }
    else if([segue.identifier isEqualToString:@"didSelectUUID"]) {
        UINavigationController *nav=segue.destinationViewController;
        SetUUIDToRange* destViewController = [[nav viewControllers] firstObject];
        destViewController.delegate=self;
        destViewController.currentUUID=[NSString stringWithFormat:@"%@",UUIDToMonitor];
    }
}

@end
