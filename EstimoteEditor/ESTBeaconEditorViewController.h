//
//  ESTBeaconEditorViewController.h
//  Examples
//
//  Created by Tod Dooshik Chung on 5/16/14.
//  Copyright (c) 2014 com.estimote. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESTBeacon.h"

@interface ESTBeaconEditorViewController : UIViewController{
    ESTBeacon *beacon;
    BOOL proximityWritingComplete;
    BOOL majorWritingComplete;
    BOOL minorWritingComplete;
    IBOutlet UITextField* uuidTextField;
    IBOutlet UITextField* majorTextField;
    IBOutlet UITextField* minorTextField;
    IBOutlet UIButton* connectionButton;
}

- (IBAction)didTouchUpdate:(id)sender;
@property (nonatomic, strong) ESTBeacon         *beacon;

@end
